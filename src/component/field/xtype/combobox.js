/**
 * Created by xiey on 2016/3/16.
 */
as.directive('comboboxField', [function () {
    return {
        restrict: 'EA',
        scope   : {},
        require : {
            ngModel: '^^ngModel',
            field  : '^^aField'
        },
        template: '<a-select ng-model="model" ng-change="change()" data="data" name="{{name}}" display-field="{{displyField}}" value-field="{{valueField}}" api="{{api}}" param="param" placeholder="{{placeholder}}" classes="fluid"></a-select>',
        link    : function (scope, el, attr, req) {
            var fieldCfg = req.field.getFieldCfg();
            scope.data = fieldCfg.data;
            scope.name = fieldCfg.name;
            scope.api = fieldCfg.api;
            scope.param = fieldCfg.param;
            scope.displyField = fieldCfg.displyField;
            scope.valueField = fieldCfg.valueField;
            scope.placeholder = fieldCfg.placeholder || ('请选择' + fieldCfg.label);
            req.ngModel.$render = function () {
                scope.model = req.ngModel.$modelValue || req.ngModel.$viewValue
            };
            scope.change = function () {
                req.ngModel.$setViewValue(scope.model);
            };
        }
    };
}]);