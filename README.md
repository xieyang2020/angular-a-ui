﻿#angular-a-ui

> **文档:**

> - [官方API](https://docs.angularjs.org/api)
> - [ng 1.5 component 写法, 以及与 directive 写法的区别](https://docs.angularjs.org/guide/component)
> - [constant value service factory 区别](http://www.cnblogs.com/Leo_wl/p/4350951.html)

---
> **TODO**

> - √  配置中apiParam改成param
> - √  配置中apiParam改成param
> - √  全局字体(web全局字体)
> - √  按钮颜色
> - √  placeholder 默认值: 请选择..
> - select 默认选中第一个
> - select 默认添加 所有/全部 的情况
> - input 中添加 number, date, datetime time, float, money, email, phone, upload
> - 图标 input 添加回车点击(input focus 状态时)
> - √  form 中所有的label自动对齐
> - √  grid 序号列剧中
> - √  grid 操作列剧中 样式调整
> - √  grid 配置中的dataIndex改为name
> - 下拉多选
> - 切换皮肤(future)
> - from 中添加 日期控件(date, datetime time/ 开始-结束日期)
> - √  分页参照ext
> - 所有input的验证添加 vtype[email/phone/money/date/postcode/url]
> - 页面完整的增删改查, toast alert
> - angular 参考文档
> - component写法文档
> - json文件改名 如demo.json -> demo.json?param=1
> - model的变量名区别一下
> - 所有的配置尽量减少
> - 每个demo其相关api以及文档附带在demo上, 关于angular优秀的文档丢到md文件里
> - 参照类似网络上api设计, 首先演示标题,左面代码,右面演示结果