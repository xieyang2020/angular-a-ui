/**
 * Created by xiey on 2016/2/24.
 *
 * label: [object String]
 * readOnly: [object Boolean]
 * disabled: [object Boolean]
 * checked: [object Boolean]
 *
 * TODO half checked
 */
as.controller('$$CheckBoxCtrl', function () {
    var ctrl = this, hasNgModel = false;
    ctrl.checked = ctrl.checked || false;
    ctrl.$onInit = function () {
        hasNgModel = !!ctrl.ngModel;
        hasNgModel && (ctrl.ngModel.$render = function () {
            ctrl.checked = ctrl.ngModel.$modelValue;
        });
    };
    ctrl.click = function () {
        ctrl.checked = !ctrl.checked;
        hasNgModel && ctrl.ngModel.$setViewValue(ctrl.checked);
    };
}).component('aCheckbox', {
    require   : {
        ngModel: '?ngModel'
    },
    bindings  : {
        label   : '@?',
        name    : '@?',
        readOnly: '<?',
        disabled: '<?',
        checked : '=?'
    },
    template  : '<div class="ui checkbox" ng-click="!$ctrl.disabled && !$ctrl.readOnly && $ctrl.click()" ng-class="{\'read-only\':$ctrl.readOnly,disabled:$ctrl.disabled}"><input type="checkbox" class="hidden" name="{{::$ctrl.name}}" ng-checked="$ctrl.checked" ng-disabled="$ctrl.disabled"><label ng-bind="::$ctrl.label"></label></div>',
    controller: '$$CheckBoxCtrl'
});