/**
 * Created by xiey on 2016/4/6.
 */
(function(window, document) {
    if (window.HTMLElement) {
        HTMLElement.prototype.on = function(type, cb, isbb) {
            if (window.addEventListener) {
                this.addEventListener(type, cb, isbb || false);
            }

            return this;
        };
        HTMLElement.prototype.toggleClass = function(cls) {
            if (this.classList) {
                this.classList.toggle(cls);
            }

            return this;
        };
    }

    function select(selector) {
        return document.querySelector(selector);
    }

    var CheckBox = {
        checked: 'checked'
    };

    var CheckBoxNormal = {
        _self: select('.js_anim_normal'),
        init: function() {
            this.bind();
        },
        bind: function() {
            this._self.on('click', this.handleCheckbox.bind(this));
        },
        handleCheckbox: function(e) {
            this._self.toggleClass(CheckBox.checked);
        }
    };

    var CheckBoxHalf = {
        _self: select('.js_anim_half'),
        init: function() {
            this.bind();
        },
        bind: function() {
            this._self.on('click', this.handleCheckbox.bind(this));
        },
        handleCheckbox: function(e) {
            this._self.toggleClass(CheckBox.checked);
        }
    };

    [CheckBoxNormal, CheckBoxHalf].forEach(function(checkbox) {
        checkbox.init();
    });
})(window, document);