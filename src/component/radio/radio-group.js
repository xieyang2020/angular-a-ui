/**
 * Created by xiey on 2016/3/14.
 *
 * data: [object Array] // 静态数据
 * api: [object String] // 远程请求数据
 * param: [object Object] // 远程请求参数
 * displayField: [object String]
 * valueField: [object String]
 * name: [object String]
 *
 */
as.run(['$templateCache', function ($templateCache) {
    var content = [];
    content.push('<div class="inline fields">');
    content.push('  <div ng-repeat="item in $ctrl.$data track by $index" class="field">');
    content.push('    <a-radio ng-model="$ctrl.$model" ng-change="$ctrl.change()" name="{{::$ctrl.name}}" data="item" display-field="$$display" value-field="$$value"></a-radio>');
    content.push('  </div>');
    content.push('  <a-store api="{{$ctrl.api}}" params="$ctrl.param" ng-model="$ctrl.storeData" ng-change="$ctrl.storeChange()" state="$ctrl.loadState"></a-store>');
    content.push('  <a-loader show="$ctrl.loadState==1" size="mini"></a-loader>');
    content.push('</div>');
    $templateCache.put('radio/radio-group.html', content.join(''));
}]).controller('$$RadioGroupCtrl', [function () {
    var ctrl = this, display = '$$display', value = '$$value';
    ctrl.$data = [];
    ctrl.$onInit = function () {
        var initData = function () {
            ctrl.$data = [];
            ctrl.data.forEach(function (item, i) {
                var obj = {};
                obj[display] = !!ctrl.displayField ? item[ctrl.displayField] : item;
                obj[value] = !!ctrl.valueField ? item[ctrl.valueField] : item;
                ctrl.$data[i] = obj;
            });
        };
        var initChoose = function () {
            ctrl.$model = ctrl.model.$modelValue;
        };
        ctrl.storeChange = function () {
            if (!ctrl.storeData) {
                ctrl.data = [];
                return;
            }
            ctrl.data = ctrl.storeData.data;
            initData();
            initChoose();
        };
        ctrl.model.$render = function () {
            initChoose();
        };
        ctrl.change = function () {
            ctrl.model.$setViewValue(ctrl.$model);
        };
        if (!ctrl.api) {
            initData();
        }
    };
}]).component('aRadioGroup', {
    require    : {
        model: 'ngModel'
    },
    bindings   : {
        data        : '=?',
        api         : '@?',
        param       : '<?',
        displayField: '@?',
        valueField  : '@?',
        name        : '@?'
    },
    templateUrl: 'radio/radio-group.html',
    controller : '$$RadioGroupCtrl'
});