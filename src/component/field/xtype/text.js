/**
 * Created by xiey on 2016/3/16.
 */
as.directive('textField', [function () {
    return {
        restrict: 'EA',
        scope   : {},
        require : {
            ngModel: '^^ngModel',
            field  : '^^aField'
        },
        template: '<input type="{{type}}" class="form-control input-sm" placeholder="{{::placeholder}}">',
        replace : true,
        link    : function (scope, el, attr, req) {
            var fieldCfg = req.field.getFieldCfg(), message = fieldCfg.message;
            scope.ngModel = req.ngModel;
            scope.type = fieldCfg.type;
            scope.placeholder = fieldCfg.placeholder || ('请输入' + fieldCfg.label);
            el.on('input', function () {
                req.ngModel.$setViewValue(el.val());
            });
            if (message.minlength) {
                req.ngModel.$validators.minlength = function (modelValue, viewValue) {
                    var value = modelValue || viewValue;
                    if (value === undefined || value === '') {
                        return true;
                    }
                    return value.length >= message.minlength.value;
                };
            }
            if (message.maxlength) {
                req.ngModel.$validators.maxlength = function (modelValue, viewValue) {
                    var value = modelValue || viewValue;
                    if (value === undefined || value === '') {
                        return true;
                    }
                    return value.length <= message.maxlength.value;
                };
            }
            req.ngModel.$render = function () {
                el.val(req.ngModel.$modelValue || req.ngModel.$viewValue);
            };
        }
    };
}]);