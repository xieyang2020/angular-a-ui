/**
 * Created by xiey on 2016/3/14.
 */
as.run(['$templateCache', function ($templateCache) {
    var content = [];
    content.push('<div class="ui radio checkbox" ng-click="!$ctrl.disabled && !$ctrl.readonly && $ctrl.click()" ng-class="{\'read-only\':$ctrl.readonly,disabled:$ctrl.disabled}">');
    content.push('  <input type="radio" name="{{::$ctrl.name}}" class="hidden" ng-disabled="$ctrl.disabled" ng-readonly="$ctrl.readonly" value="{{::$ctrl.$data.$$value}}">');
    content.push('  <label ng-bind="::$ctrl.$data.$$label"></label>');
    content.push('</div>');
    $templateCache.put('radio/radio.html', content.join(''));
}]).controller('$$RadioCtrl', ['$element', '$timeout', function ($element, $timeout) {
    var ctrl = this, label = '$$label', value = '$$value', $input = $element.find('input');
    ctrl.$data = {};
    ctrl.$data[label] = !!ctrl.displayField ? ctrl.data[ctrl.displayField] : ctrl.data;
    ctrl.$data[value] = !!ctrl.valueField ? ctrl.data[ctrl.valueField] : ctrl.data;
    ctrl.$onInit = function () {
        ctrl.model.$render = function () {
            ctrl.model.$modelValue === ctrl.$data[value] && $timeout(function () {
                $input.attr('checked', true);
            });
        };
        ctrl.click = function () {
            $input.attr('checked', true);
            ctrl.model.$setViewValue(ctrl.$data[value]);
        };
    };
}]).component('aRadio', {
    require    : {
        model: 'ngModel'
    },
    bindings   : {
        data        : '<',
        name        : '@?',
        readonly    : '<?',
        disabled    : '<?',
        displayField: '@?',
        valueField  : '@?'
    },
    templateUrl: 'radio/radio.html',
    controller : '$$RadioCtrl'
});