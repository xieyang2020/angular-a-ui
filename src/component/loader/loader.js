as.component('aLoader', {
    bindings: {
        msg : '@?',
        size: '@?',
        show: '<'
    },
    template: '<div ng-if="$ctrl.show" class="ui active inverted dimmer"><div class="ui loader {{$ctrl.size}}" ng-class="{text:!!$ctrl.msg}">{{$ctrl.msg}}</div></div>'
});