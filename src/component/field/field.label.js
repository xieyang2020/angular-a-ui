/**
 * Created by xiey on 2016/3/16.
 *
 * 每个 label 对齐
 */
as.directive('fieldLabel', function () {
    return {
        require : '^^aField',
        scope   : {},
        template: '<span ng-repeat="item in ::labels" ng-bind="::item"></span>',
        link    : function (scope, el, attr, field) {
            scope.labels = field.getFieldCfg().label.split('');
        }
    };
});