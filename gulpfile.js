/**
 * Created by xiey on 2016/3/29.
 */
var del      = require('del'),
    gulp     = require('gulp'),
    $        = require('gulp-load-plugins')(),
    gulpSync = $.sync(gulp);

var PATH = {
    BUILD    : './build',
    COMPONENT: ['./src/a.ui.js', './src/component/**'],
    STYLE    : ['./src/style/css/**', './src/component/**'],
    FONT     : './src/style/fonts/**'
};

gulp.task('clean', function (done) {
    return del(PATH.BUILD, done);
});

gulp.task('minify:js', function () {
    var jsFilter = $.filter('**/*.js');
    return gulp.src(PATH.COMPONENT)
        .pipe(jsFilter)
        .pipe($.concat('a.ui.js'))
        .pipe($.header('(function(window, angular, undefined) {\'use strict\';\n'))
        .pipe($.footer('\n}(window, angular));'))
        .pipe($.uglify())
        .pipe(gulp.dest(PATH.BUILD + '/js/'))
});

gulp.task('minify:css', function () {
    var cssFilter = $.filter('**/*.css');
    return gulp.src([].concat(PATH.STYLE, PATH.COMPONENT))
        .pipe(cssFilter)
        .pipe($.concat('a.ui.css'))
        .pipe($.cleanCss())
        .pipe(gulp.dest(PATH.BUILD + '/css/'));
});

gulp.task('copy:font', function () {
    return gulp.src(PATH.FONT)
        .pipe(gulp.dest(PATH.BUILD + '/fonts/'));
});

gulp.task('minify', gulpSync.sync(['minify:js', 'minify:css']));
gulp.task('copy', gulpSync.sync(['copy:font']));

gulp.task('default', gulpSync.sync(['clean', 'minify', 'copy']));