/**
 * Created by xiey on 2016/3/29.
 */
angular.module('app', ['a.ui'])
    .controller('ToastDemoCtrl', ['$scope', '$toast', function ($scope, $toast) {
        $scope.title = 'Toaster';
        $scope.types = ['info', 'error', 'success', 'wait', 'warn'];
        $scope.msg = 'Message';
        $toast.wait('正在加载中..');
        $scope.toast = function (type) {
            $toast[type]($scope.msg);
        };
        $scope.clearToasts = function () {
            $toast.clear();
        }
    }])
    .controller('DialogDemoCtrl', ['$scope', '$dialog', function ($scope, $dialog) {
        $scope.title = 'Dialog';
        $scope.msg = 'Message';
        $scope.types = ['alert', 'confirm', 'prompt'];
        $scope.dialog = function (type) {
            $dialog[type]($scope.msg, function (arg) {
                console.log(arg);
            });
        };
    }]);