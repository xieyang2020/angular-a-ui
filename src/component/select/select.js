/**
 * Created by xiey on 2016/2/29.
 */
as.run(['$templateCache', function ($templateCache) {
    var content = [];
    content.push('<div class="ui selection dropdown {{$ctrl.classes}}" ng-click="$ctrl.dropdown()" ng-class="{\'active visible\': $ctrl.active, \'loading disabled\': $ctrl.loadState==1, multiple: $ctrl.multiple}" any-other-click="$ctrl.close()" ng-switch="$ctrl.multiple">');
    content.push('  <input type="text" class="hidden" ng-model="$ctrl.$model" name="{{::$ctrl.name}}">');
    content.push('  <i class="dropdown icon"></i>');
    content.push('  <div ng-if="!$ctrl.multiple || $ctrl.currentSelect.length==0" ng-bind="$ctrl.currentSelect[0]?$ctrl.currentSelect[0][$ctrl.displayField]:$ctrl.placeholder" ng-class="{default: $ctrl.multiple || !$ctrl.currentSelect[0]}" class="text"></div>');
    content.push('  <a ng-switch-when="true" ng-repeat="sel in $ctrl.currentSelect track by $index" class="ui label transition">{{sel[$ctrl.displayField]}}<i ng-click="$ctrl.removeSel(sel, $event)" class="glyphicon glyphicon-remove"></i></a>');
    content.push('  <div class="menu transition" ng-class="{hidden:!$ctrl.active,visible:$ctrl.active}">');
    content.push('    <div class="item" ng-repeat="item in ($ctrl.$data|exclude:$ctrl.currentSelect) track by $index" ng-bind="item[$ctrl.displayField]" ng-click="$ctrl.select(item)" ng-class="{active:item===$ctrl.currentSelect}"></div>');
    content.push('  </div>');
    content.push('  <a-store api="{{$ctrl.api}}" params="$ctrl.param" ng-model="$ctrl.storeData" ng-change="$ctrl.dataChange()" state="$ctrl.loadState"></a-store>');
    content.push('</div>');
    $templateCache.put('select/select.html', content.join(''));
}]).filter('exclude', [function () {
    return function (input, excludes) {
        var output = [];
        input.forEach(function (item) {
            if (!isInclude(excludes, item)) {
                output.push(item);
            }
        });
        return output;
    };
}]).controller('$$SelectCtrl', ['$scope', function ($scope) {
    var ctrl = this, hasDisplayField = !!ctrl.displayField, hasValueField = !!ctrl.valueField, VALUE = '$value', DISPLAY = '$display';
    ctrl.active = false;
    ctrl.currentSelect = [];
    ctrl.data = ctrl.data || [];
    ctrl.multiple = !!ctrl.multiple;
    ctrl.displayField = ctrl.displayField || DISPLAY;
    ctrl.valueField = ctrl.valueField || VALUE;
    ctrl.placeholder = ctrl.placeholder || '请选择..';
    ctrl.$onInit = function () {
        var model = ctrl.model;
        var setAsCurrentSelectValue = function () {
            var value = ctrl.currentSelect.map(function (item) {
                return item[ctrl.valueField];
            });
            if (!ctrl.multiple) {
                value = value[0];
            }
            ctrl.$model = value;
            model.$setViewValue(value);
        };
        var initSelect = function () {
            ctrl.currentSelect = [];
            var modelValue = model.$modelValue || [];
            if (!ctrl.multiple && modelValue) {
                modelValue = [modelValue];
            }
            ctrl.$data.forEach(function (item) {
                if (isInclude(modelValue, item[ctrl.valueField])) {
                    ctrl.currentSelect.push(item);
                }
            });
            //ctrl.currentSelect.length > 0 && setAsCurrentSelectValue();
        };
        $scope.$watchCollection('$ctrl.data', function (data) {
            ctrl.$data = [];
            data && data.forEach(function (item) {
                var current = {};
                // 格式全部统一成含有 valueField 和 disPlayField
                current[ctrl.displayField] = (hasDisplayField) ? item[ctrl.displayField] : item;
                current[ctrl.valueField] = (hasValueField) ? item[ctrl.valueField] : item;
                ctrl.$data.push(current);
            });
            ctrl.hasData = ctrl.$data.length > 0;
            initSelect();
        });
        ctrl.dataChange = function () {
            if (!ctrl.storeData) {
                ctrl.data = [];
                return;
            }
            ctrl.data = ctrl.storeData.data;
        };
        ctrl.dropdown = function () {
            if (ctrl.loadState === 1) {
                return;
            }
            if (ctrl.multiple) {
                ctrl.active = true;
            } else {
                ctrl.active = !ctrl.active;
            }
        };
        ctrl.select = function (item) {
            if (ctrl.multiple) {
                ctrl.currentSelect.push(item);
            } else {
                ctrl.currentSelect = [item];
            }
            setAsCurrentSelectValue();
        };
        ctrl.close = function () {
            ctrl.active = false;
        };
        ctrl.removeSel = function (sel, $event) {
            ctrl.currentSelect.splice(ctrl.currentSelect.indexOf(sel), 1);
            setAsCurrentSelectValue();
            $event.stopPropagation();
        };
    };
}]).component('aSelect', {
    require    : {
        model: 'ngModel'
    },
    bindings   : {
        name        : '@?',
        data        : '<?',
        api         : '@?',
        param       : '<?',
        placeholder : '@?',
        displayField: '@?',
        valueField  : '@?',
        classes     : '@?',
        multiple    : '<?'
    },
    templateUrl: 'select/select.html',
    controller : '$$SelectCtrl'
});