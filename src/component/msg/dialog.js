/**
 * Created by xiey on 2016/3/23.
 */
as.run(['$templateCache', function ($templateCache) {
    var dialog = [];
    dialog.push('<div class="dialog-title">');
    dialog.push('  <h3>{{::dialog.title}}</h3>');
    dialog.push('</div>');
    dialog.push('<p class="dialog-content">');
    dialog.push('  <span class="dialog-icon icon" ng-if="::!!dialog.icon"><i class="glyphicon" ng-class="::dialog.icon"></i></span>');
    dialog.push('  <span class="msg-content">');
    dialog.push('    {{::dialog.msg}}');
    dialog.push('    <input ng-if="::dialog.type==\'prompt\'" class="form-control" ng-model="dialog.input">');
    dialog.push('  </span>');
    dialog.push('</p>');
    dialog.push('<div class="dialog-actions">');
    dialog.push('  <button ng-repeat="btn in ::dialog.btns track by $index" class="btn btn-default" ng-class="::btn.icon" type="button" ng-click="dialog.btnClick($index)">{{::btn.title}}</button>');
    dialog.push('</div>');
    $templateCache.put('dialog/dialog.html', dialog.join(''));
}]).service('$dialog', ['$win', function ($win) {
    var types = {
        alert  : {
            title  : '提示',
            icon   : 'glyphicon-info-sign',
            buttons: [{
                title: '确认'
            }]
        },
        confirm: {
            title  : '请确认',
            icon   : 'glyphicon-question-sign',
            buttons: [{
                title: '确认',
                icon : 'btn-info'
            }, {
                title: '取消'
            }]
        },
        prompt : {
            title  : '请输入',
            buttons: [{
                title: '确认',
                icon : 'btn-info'
            }, {
                title: '取消'
            }]
        }
    };
    var open = function (msg, callback, type) {
        var t = types[type];
        if (!t) {
            t = types.alert;
        }
        var w = $win.open({
            width       : 360,
            template    : 'dialog/dialog.html',
            controllerAs: 'dialog',
            controller  : function () {
                var ctrl = this;
                ctrl.title = t.title;
                ctrl.type = type;
                ctrl.icon = t.icon;
                ctrl.msg = msg;
                ctrl.btns = t.buttons;
                ctrl.btnClick = function (index) {
                    callback(index, ctrl.input);
                    w.close();
                };
            }
        });
    };
    var alert = function (msg, callback) {
        var fn = function () {
            callback(true);
        };
        open(msg, fn, 'alert');
    };
    var confirm = function (msg, callback) {
        var fn = function (index) {
            callback(index === 0);
        };
        open(msg, fn, 'confirm');
    };
    var prompt = function (msg, callback) {
        var fn = function (index, input) {
            index === 1 && (input = '');
            callback(input);
        };
        open(msg, fn, 'prompt');
    };
    return {
        alert  : alert,
        confirm: confirm,
        prompt : prompt
    };
}]);