/*
 * angular-ui-bootstrap
 * http://angular-ui.github.io/bootstrap/

 * Version: 1.2.5 - 2016-03-20
 * License: MIT
 */

as.constant('$$RATING_CONFIG', {
    max        : 5,
    stateOn    : null,
    stateOff   : null,
    enableReset: true,
    titles     : ['1', '2', '3', '4', '5']
}).run(["$templateCache", function ($templateCache) {
    $templateCache.put("rating/rating.html",
        "<span ng-mouseleave=\"reset()\" ng-keydown=\"onKeydown($event)\" tabindex=\"0\" role=\"slider\" aria-valuemin=\"0\" aria-valuemax=\"{{range.length}}\" aria-valuenow=\"{{value}}\" aria-valuetext=\"{{title}}\" class=\"rating\">\n" +
        "    <span ng-repeat-start=\"r in range track by $index\" class=\"sr-only\">({{ $index < value ? '*' : ' ' }})</span>\n" +
        "    <i ng-repeat-end ng-mouseenter=\"enter($index + 1)\" ng-click=\"rate($index + 1)\" class=\"glyphicon\" ng-class=\"$index < value && (r.stateOn || 'glyphicon-star') || (r.stateOff || 'glyphicon-star-empty')\" ng-attr-title=\"{{r.title}}\"></i>\n" +
        "</span>");
}]).controller('$$RatingController', ['$scope', '$attrs', '$$RATING_CONFIG', function ($scope, $attrs, $$RATING_CONFIG) {
        var ngModelCtrl = {$setViewValue: angular.noop},
            self        = this;

        this.init = function (ngModelCtrl_) {
            ngModelCtrl = ngModelCtrl_;
            ngModelCtrl.$render = this.render;

            ngModelCtrl.$formatters.push(function (value) {
                if (angular.isNumber(value) && value << 0 !== value) {
                    value = Math.round(value);
                }

                return value;
            });

            this.stateOn = angular.isDefined($attrs.stateOn) ? $scope.$parent.$eval($attrs.stateOn) : $$RATING_CONFIG.stateOn;
            this.stateOff = angular.isDefined($attrs.stateOff) ? $scope.$parent.$eval($attrs.stateOff) : $$RATING_CONFIG.stateOff;
            this.enableReset = angular.isDefined($attrs.enableReset) ?
                $scope.$parent.$eval($attrs.enableReset) : $$RATING_CONFIG.enableReset;
            var tmpTitles = angular.isDefined($attrs.titles) ? $scope.$parent.$eval($attrs.titles) : $$RATING_CONFIG.titles;
            this.titles = angular.isArray(tmpTitles) && tmpTitles.length > 0 ?
                tmpTitles : $$RATING_CONFIG.titles;

            var ratingStates = angular.isDefined($attrs.ratingStates) ?
                $scope.$parent.$eval($attrs.ratingStates) :
                new Array(angular.isDefined($attrs.max) ? $scope.$parent.$eval($attrs.max) : $$RATING_CONFIG.max);
            $scope.range = this.buildTemplateObjects(ratingStates);
        };

        this.buildTemplateObjects = function (states) {
            for (var i = 0, n = states.length; i < n; i++) {
                states[i] = angular.extend({index: i}, {stateOn: this.stateOn, stateOff: this.stateOff, title: this.getTitle(i)}, states[i]);
            }
            return states;
        };

        this.getTitle = function (index) {
            if (index >= this.titles.length) {
                return index + 1;
            }

            return this.titles[index];
        };

        $scope.rate = function (value) {
            if (!$scope.readonly && value >= 0 && value <= $scope.range.length) {
                var newViewValue = self.enableReset && ngModelCtrl.$viewValue === value ? 0 : value;
                ngModelCtrl.$setViewValue(newViewValue);
                ngModelCtrl.$render();
            }
        };

        $scope.enter = function (value) {
            if (!$scope.readonly) {
                $scope.value = value;
            }
            $scope.onHover({value: value});
        };

        $scope.reset = function () {
            $scope.value = ngModelCtrl.$viewValue;
            $scope.onLeave();
        };

        $scope.onKeydown = function (evt) {
            if (/(37|38|39|40)/.test(evt.which)) {
                evt.preventDefault();
                evt.stopPropagation();
                $scope.rate($scope.value + (evt.which === 38 || evt.which === 39 ? 1 : -1));
            }
        };

        this.render = function () {
            $scope.value = ngModelCtrl.$viewValue;
            $scope.title = self.getTitle($scope.value - 1);
        };
    }])

    .directive('aRating', function () {
        return {
            require    : ['aRating', 'ngModel'],
            scope      : {
                readonly: '=?readOnly',
                onHover : '&',
                onLeave : '&'
            },
            controller : '$$RatingController',
            templateUrl: 'rating/rating.html',
            replace    : true,
            link       : function (scope, element, attrs, ctrls) {
                var ratingCtrl = ctrls[0], ngModelCtrl = ctrls[1];
                ratingCtrl.init(ngModelCtrl);
            }
        };
    });