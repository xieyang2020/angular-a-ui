/**
 * Created by xiey on 2016/2/26.
 */
as.run(['$templateCache', function ($templateCache) {
    var pagination = [];
    pagination.push('<div class="a-pagination">');
    pagination.push('  <div class="pagination alternate">');
    pagination.push('    <ul>');
    pagination.push('      <li ng-repeat="(key, page) in $ctrl.pages.header track by $index" ng-if="page.show" ng-class="{disabled:page.disabled}"><a ng-click="!page.disabled && $ctrl.go(page.text)">{{page.text}}</a></li>');
    pagination.push('      <li ng-repeat="(key, page) in $ctrl.pages.middle track by $index" ng-class="{active: page.text == $ctrl.current}"><a ng-click="$ctrl.go(page.text)">{{page.text}}</a></li>');
    pagination.push('      <li ng-if="!$ctrl.totalPage" class="disabled"><a>第{{$ctrl.current}}页</a></li>');
    pagination.push('      <li ng-repeat="(key, page) in $ctrl.pages.footer track by $index" ng-if="page.show" ng-class="{disabled:page.disabled}"><a ng-click="!page.disabled && $ctrl.go(page.text)">{{page.text}}</a></li>');
    pagination.push('    </ul>');
    pagination.push('  </div>');
    pagination.push('</div>');

    $templateCache.put('pagination/pagination.html', pagination.join(''));
}]).controller('$$paginationCtrl', ['$scope', function ($scope) {
    var ctrl = this, SHOW_MAX_PAGE = 5, count = Math.ceil((SHOW_MAX_PAGE - 1) / 2);
    ctrl.pages = {};
    ctrl.pages.header = {};
    ctrl.pages.footer = {};
    ctrl.pages.middle = {};
    ctrl.prevText = ctrl.prevText || '上一页';
    ctrl.nextText = ctrl.nextText || '下一页';
    ctrl.totalPage = ctrl.totalPage || 0;
    ctrl.pages.header.first = {
        text    : ctrl.firstText,
        disabled: true,
        show    : !!ctrl.firstText
    };
    ctrl.pages.header.prev = {
        text    : ctrl.prevText,
        disabled: false,
        show    : true
    };
    ctrl.pages.footer.next = {
        text    : ctrl.nextText,
        disabled: false,
        show    : true
    };
    ctrl.pages.footer.last = {
        text    : ctrl.lastText,
        disabled: false,
        show    : !!ctrl.lastText
    };
    var genPages = function () {
        var current = ctrl.current, totalPage = ctrl.totalPage;
        ctrl.pages.middle = {};
        if (totalPage <= SHOW_MAX_PAGE) {
            for (var i = 1; i <= totalPage; i++) {
                ctrl.pages.middle[i] = {
                    text: i
                };
            }
            return;
        }
        if (current <= count) {
            for (var j = 1; j <= SHOW_MAX_PAGE; j++) {
                ctrl.pages.middle[j] = {
                    text: j
                };
            }
            return;
        }
        if (count > (totalPage - current)) {
            for (var k = (totalPage - SHOW_MAX_PAGE + 1); k <= totalPage; k++) {
                ctrl.pages.middle[k] = {
                    text: k
                };
            }
            return;
        }
        for (var l = current - count; l <= current + count; l++) {
            ctrl.pages.middle[l] = {
                text: l
            };
        }
    };
    ctrl.$onInit = function () {
        var setPage = function (num) {
            if (num < 1) {
                return;
            }
            if (ctrl.totalPage && num > ctrl.totalPage) {
                return;
            }
            ctrl.current = num;
        };
        ctrl.ngModel.$render = function () {
            if (!ctrl.ngModel.$modelValue) {
                ctrl.ngModel.$setViewValue(1);
            }
            ctrl.current = ctrl.ngModel.$modelValue;
        };
        ctrl.go = function (arg) {
            if (arg === ctrl.firstText) {
                setPage(1);
            } else if (arg === ctrl.lastText) {
                setPage(ctrl.totalPage || (ctrl.current + 1));
            } else if (arg === ctrl.prevText) {
                setPage(ctrl.current - 1);
            } else if (arg === ctrl.nextText) {
                setPage(ctrl.current + 1);
            } else {
                setPage(arg);
            }
        };
        $scope.$watch('$ctrl.current', function () {
            ctrl.ngModel.$setViewValue(ctrl.current);
            ctrl.pages.header.first.disabled = ctrl.current === 1;
            ctrl.pages.header.prev.disabled = ctrl.current === 1;
            if (ctrl.totalPage) {
                ctrl.pages.footer.last.disabled = ctrl.current === ctrl.totalPage;
                ctrl.pages.footer.next.disabled = ctrl.current === ctrl.totalPage;
                genPages();
            }
        });
        $scope.$watch('$ctrl.last', function () {
            ctrl.pages.footer.last.disabled = ctrl.last;
            ctrl.pages.footer.next.disabled = ctrl.last;
        });
        $scope.$watch('$ctrl.totalPage', function (nv) {
            nv && (genPages());
        });
    };
}]).component('aPagination', {
    require    : {
        ngModel: 'ngModel'
    },
    bindings   : {
        firstText: '@',
        lastText : '@',
        prevText : '@',
        nextText : '@',
        last     : '<',
        totalPage: '<'
    },
    templateUrl: 'pagination/pagination.html',
    controller : '$$paginationCtrl'
});